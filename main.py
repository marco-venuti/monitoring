#!/usr/bin/env python3

from os import getloadavg, environ
from psutil import (
    virtual_memory, swap_memory, sensors_temperatures,
    sensors_fans, sensors_battery
)
from sqlalchemy import (
    create_engine,
    Column, Integer, DateTime, Float,
    func,
)
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import schedule
from time import sleep
from decouple import config
from datetime import datetime, timedelta
from subprocess import Popen, PIPE
from re import search
from functools import reduce

Base = declarative_base()


def get_display():
    p = Popen('who', stdout=PIPE)
    out = str(p.stdout.read())
    match = search(r'\(([:0-9]+)\)', out)
    if match:
        return match.group(1)
    else:
        return None


def get_temps():
    return [i.current for i in sensors_temperatures()['coretemp']]


def get_fan_speed():
    return sensors_fans()['thinkpad'][0].current


def get_battery_percentages(low_thr: int):
    batteries = sensors_battery()
    levels = [bat[1].percent for bat in batteries]
    if reduce(lambda x, y: x and (y <= low_thr), levels, True):
        battery_low_alert()
    return levels


def battery_low_alert():
    Popen(
        [
            'notify-send',
            'Battery critically low!',
            'Please connect charger.',
            '--icon=battery-caution'
        ],
        env=dict(environ, DISPLAY=get_display())
    )


class Record(Base):
    __tablename__ = 'main'

    id = Column(Integer, primary_key=True)
    datetime = Column(DateTime, default=func.now())
    ram = Column(Float)
    swap = Column(Float)
    loadavg = Column(ARRAY(Float))
    temp = Column(ARRAY(Float))
    fan = Column(Float)
    battery = Column(ARRAY(Float))


class Logger():
    def __init__(self, username: str, password: str, hostname: str,
                 port: int, dbname: str, interval: int = 120,
                 bat_low_thr: int = 10) -> None:
        self.interval = interval
        self.bat_low_thr = bat_low_thr
        url = f'postgresql://{username}:{password}@{hostname}:{port}/{dbname}'
        self.engine = create_engine(url, client_encoding='utf8')
        session = sessionmaker()
        session.configure(bind=self.engine)
        Base.metadata.create_all(self.engine)
        self.s = session()

    def poll(self) -> None:
        self.s.add(Record(
            ram=virtual_memory().percent,
            swap=swap_memory().percent,
            loadavg=getloadavg(),
            temp=get_temps(),
            fan=get_fan_speed(),
            battery=get_battery_percentages(self.bat_low_thr),
        ))
        self.s.commit()
        print('Saved')

    def start_polling(self) -> None:
        print('Stating polling...')
        schedule.every(self.interval).seconds.do(self.poll)
        while True:
            schedule.run_pending()
            sleep(self.interval)

    def get_latest(self, seconds: int):
        now = datetime.now()
        seconds_ago = now - timedelta(seconds=seconds)
        return self.s.query(Record).filter(
            Record.datetime > seconds_ago).order_by(Record.datetime)


if __name__ == '__main__':
    logger = Logger(config('USERNAMEDB'),
                    config('PASSWORD'),
                    config('HOSTNAME'),
                    config('PORT'),
                    config('DBNAME'),
                    config('INTERVAL', cast=int),
                    config('LOW_THR', cast=int))
    logger.start_polling()
