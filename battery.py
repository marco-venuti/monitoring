#!/usr/bin/env python3

from main import Logger
import matplotlib.pyplot as plt
from decouple import config
from sys import argv
import numpy as np


logger = Logger(config('USERNAMEDB'),
                config('PASSWORD'),
                config('HOSTNAME'),
                config('PORT'),
                config('DBNAME'),
                config('INTERVAL'))

# Prendo i dati
dates = []
bat1 = []
bat2 = []

for i in logger.get_latest(86400*float(argv[1])):
    dates.append(i.datetime)

    try:
        bat1.append(i.battery[0])
    except:
        bat1.append(0)

    try:
        bat2.append(i.battery[1])
    except:
        bat2.append(0)

rates = []

for bat in [bat1, bat2]:
    # Estraggo le cariche e le scariche
    charges = []
    charge = []
    ch_date = []

    for i in range(len(dates)-1):
        if bat[i+1] > bat[i] and (dates[i+1] - dates[i]).total_seconds() < 150:
            charge.append(bat[i])
            ch_date.append(dates[i])
        else:
            if len(charge) > 5:
                charges.append([ch_date, charge])
            charge = []
            ch_date = []

    discharges = []
    charge = []
    ch_date = []

    for i in range(len(dates)-1):
        if bat[i+1] < bat[i] and (dates[i+1] - dates[i]).total_seconds() < 150:
            charge.append(bat[i])
            ch_date.append(dates[i])
        else:
            if len(charge) > 5:
                discharges.append([ch_date, charge])
            charge = []
            ch_date = []


    # normalizzo le cariche e le scariche
    def normalize(x):
        return [
            [
                ch[0][0],  # data di inizio
                np.array([(c - ch[0][0]).total_seconds() for c in ch[0]]),  # secondi dall'inizio
                np.array(ch[1])  # percentuale batteria
            ]
            for ch in x
        ]


    charges = normalize(charges)
    discharges = normalize(discharges)


    # Calcolo i rate di carica e scarica
    def fit_m(x, y):
        return (
            (x*y).sum() - x.sum()*y.sum()/len(x)
        ) / (
            (x*x).sum() - (x.sum())**2/len(x)
        )


    def get_rates(x):
        """
        Returns a tuple with ((dis)charge started, slope given by least square fit),
        in units of %/minute.
        """
        return (
            [ch[0] for ch in x],
            [
                fit_m(ch[1], ch[2])*60
                for ch in x
            ]
        )

    rates.append([get_rates(charges), get_rates(discharges)])

plt.style.use('style.yml')
plt.plot_date(*rates[0][0], marker='x', color='red', label='BAT 0')
plt.plot_date(*rates[0][1], marker='x', color='red')
plt.plot_date(*rates[1][0], marker='x', color='blue', label='BAT 1')
plt.plot_date(*rates[1][1], marker='x', color='blue')
plt.legend()
plt.xlabel('Date')
plt.ylabel(r'\%/min')
plt.show()
