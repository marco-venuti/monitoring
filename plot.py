#!/usr/bin/env python3

from main import Logger
import matplotlib.pyplot as plt
from decouple import config
from sys import argv

logger = Logger(config('USERNAMEDB'),
                config('PASSWORD'),
                config('HOSTNAME'),
                config('PORT'),
                config('DBNAME'),
                config('INTERVAL'))
dates = []
ram = []
swap = []
loadavg = []
temp = []
fan = []
batteries = []

for i in logger.get_latest(86400*float(argv[1])):
    dates.append(i.datetime)
    ram.append(i.ram)
    swap.append(i.swap)
    loadavg.append(i.loadavg)
    temp.append(i.temp[:3] if i.temp else 3*[0])
    fan.append(i.fan if i.fan else 0)

    toAppend = []
    for j in range(2):
        try:
            toAppend.append(i.battery[j])
        except IndexError:
            toAppend.append(0)
        except TypeError:
            toAppend = [0, 0]
    batteries.append(toAppend)

# Faccio i grafici
plt.style.use('style.yml')

fig, axes = plt.subplots(
    5, 1, sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios': 5*[1]},
)

style = {
    'linestyle': '-',
    'marker': ''
}

# Ram/swap
axes[0].plot_date(dates, ram, **style, color='blue')
axes[0].plot_date(dates, swap, **style, color='red')
axes[0].legend(['Ram', 'Swap'], loc='upper left')
axes[0].set_ylim(0, 100)
axes[0].set_ylabel('Percentage')

# Load
axes[1].plot_date(dates, loadavg, **style)
axes[1].legend(["5'", "10'", "15'"], loc='upper left')
axes[1].set_ylabel('Load average')

# Temp
axes[2].plot_date(dates, temp, **style)
axes[2].legend(['1', '2', '3'], loc='upper left')
axes[2].set_ylabel('Temperature [°C]')

# Fan
axes[3].plot_date(dates, fan, **style)
axes[3].set_ylabel('Fan speed [RPM]')
axes[3].set_ylim(-100, 4500)

# Batteries
axes[4].plot_date(dates, batteries, **style)
axes[4].legend(['BAT0', 'BAT1'], loc='upper left')
axes[4].set_ylabel('Percentage')
axes[4].set_ylim(0, 100)
axes[-1].set_xlabel('Date')

plt.show()
